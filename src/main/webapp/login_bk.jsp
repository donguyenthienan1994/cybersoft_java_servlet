<%@ page isELIgnored="false" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page import="model.Users" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<%--
    <%! %> : Thẻ dùng để khai báo biến
    <% %> : Thẻ dùng để xử lý logic code
    <%= %> : Thẻ dùng để xuất giá trị biến ra màn hình
--%>
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
<%!
    int count = 0;
    Users users = new Users();

%>

<%
    String name = (String) request.getAttribute("name");
    count++;
    users.setUsername("An Do");
    users.setPassword("123456");
%>
<b><%= count %></b>
<%
    if(count%2 == 0){
%>

<b><%=
    name
%></b>
<% } %>

<div class="container">
  <div class="row mt-5">
    <div class="col-md-5 m-auto mt-5">
      <h3 class="text-center">ĐĂNG NHẬP HỆ THỐNG</h3>
      <div class="p-4 border mt-4">
      <%--
        action: Duong dan ma tham so se duoc gui ve
        method: Phuong thuc gui tham so cho duong dan khai bao o action
      --%>
      <% String contextPath = request.getContextPath(); %>
      <h1>${request.getContextPath()}</h1>
      <b>${user.username}</b>
      <c:out value = "${user.password}" />
      <c:if test="${user.password == '123'}">
        Mat khau cua ban la 123
      </c:if>
      <c:forEach var = "i" begin = "0" end = "${list.size() - 1}">
               <c:out value = "${list.get(i)}"/><p>
      </c:forEach>
      <c:forEach var = "str" items = "${list}">
                     <b>${str}</b>
      </c:forEach>
        <form action="<%= contextPath%>/login" method = "post">
            <div class="form-group">
              <label>Email</label>
              <input type="email" class="form-control" name="email">
            </div>
            <div class="form-group">
              <label>Mật khẩu</label>
              <input type="password" class="form-control" name="password">
            </div>
            <button type="submit" class="btn btn-primary">Đăng nhập</button>
          </form>
      </div>
      </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
