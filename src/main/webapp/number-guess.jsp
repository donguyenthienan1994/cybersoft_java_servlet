<%@ page isELIgnored="false" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page import="model.Gamer" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<%--
    <%! %> : Thẻ dùng để khai báo biến
    <% %> : Thẻ dùng để xử lý logic code
    <%= %> : Thẻ dùng để xuất giá trị biến ra màn hình
--%>
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
<%!
    int count = 0;
%>

<%
    count++;
%>

<div class="container">
  <div class="row mt-5">
    <div class="col-md-5 m-auto mt-5">
      <h3 class="text-center">Luot choi cua ${currentGamer.name}</h3>
      <div class="p-4 border mt-4">
      <%--
        action: Duong dan ma tham so se duoc gui ve
        method: Phuong thuc gui tham so cho duong dan khai bao o action
      --%>
      <% String contextPath = request.getContextPath(); %>
        <form action="<%= contextPath%>/numberGuess" method = "post">
            <div class="form-group">
              <label>Moi ban ${currentGamer.name} doan so tu 0 - 1000</label>
              <input type="guessNumber" class="form-control" name="guessNumber">
            </div>
            <button type="submit" class="btn btn-primary">Guess</button>
        </form>

        <c:if test = "${guessNumber > 0 && guessNumber == number}">
            <form action="<%= contextPath%>/inputGamer" method = "get">
                <button type="submit" class="btn btn-primary">Người chơi tiếp theo >></button>
            </form>
         </c:if>

      </div>
      <c:if test = "${guessNumber > 0 && guessNumber > number}">
            <p>Số bạn đoán lớn hơn. Hãy thử lần nữa<p>
      </c:if>
      <c:if test = "${guessNumber > 0 && guessNumber < number}">
             <p>Số bạn đoán nhỏ hơn. Hãy thử lần nữa<p>
      </c:if>
      <c:if test = "${guessNumber > 0 && guessNumber == number}">
            <p>Xin chúc mừng bạn ${currentGamer.name} đã đoán đúng trong ${currentGamer.guessCount} lần đoán<p>
            <p>Bạn đang đứng hạng ${currentRank}<p>
     </c:if>
      </div>
  </div>
</div>

<table>
  <tr>
    <th>Hạng</th>
    <th>Tên người chơi</th>
    <th>Số lần đoán</th>
  </tr>
    <c:forEach var = "gamer" items = "${gamerList}">
               <tr>
                   <td><c:out value="${gamerList.indexOf(gamer) + 1}"/></td>
                   <td>${gamer.name}</td>
                   <td>${gamer.getGuessCount()}</td>
                 </tr>
      </c:forEach>
</table>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
