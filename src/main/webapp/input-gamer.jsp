<%@ page isELIgnored="false" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page import="model.Product" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<%--
    <%! %> : Thẻ dùng để khai báo biến
    <% %> : Thẻ dùng để xử lý logic code
    <%= %> : Thẻ dùng để xuất giá trị biến ra màn hình
--%>
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>

<div class="container">
  <div class="row mt-5">
    <div class="col-md-5 m-auto mt-5">
      <h3 class="text-center">Nhap ten nguoi choi</h3>
      <div class="p-4 border mt-4">
      <%--
        action: Duong dan ma tham so se duoc gui ve
        method: Phuong thuc gui tham so cho duong dan khai bao o action
      --%>
      <% String contextPath = request.getContextPath(); %>
        <form action="<%= contextPath%>/numberGuess" method = "post">
            <div class="form-group">
              <label>Ten nguoi choi</label>
              <input type="name" class="form-control" name="name">
            </div>
            <button type="submit" class="btn btn-primary">Luu lai</button>
          </form>
      </div>
      </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
