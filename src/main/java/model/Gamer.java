package model;

public class Gamer {
    private String name;

    public String getName() {
        return name;
    }

    private int guessCount;

    public int getGuessCount() {
        return guessCount;
    }

    public void setGuessCount(int guessCount) {
        this.guessCount = guessCount;
    }

    public Gamer(String name){
        this.name = name;
    }
}
