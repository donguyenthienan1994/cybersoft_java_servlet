package filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
//urlPattern : khi nguoi dung goi link dc qui dinh trong urlPattern thi filter se duoc kich hoat
@WebFilter(urlPatterns = {"/login"})
public class AuthFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest) servletRequest).getSession();
        boolean isLogin = session.getAttributeNames().hasMoreElements() && (boolean) session.getAttribute("s_isLogin");
        //Nơi qui định rule filter
        System.out.println("Filter duoc kich hoat !!!");
        System.out.println(isLogin ? "User chua login" : "User da login");
        // Cho phep di vao link ng dung muon vao sau khi pass filter
        if(!isLogin){
            filterChain.doFilter(servletRequest,servletResponse);
        } else {
            servletRequest.getRequestDispatcher("/user").forward(servletRequest,servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
