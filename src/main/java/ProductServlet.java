import model.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "productServlet", urlPatterns = {"/product"})
public class ProductServlet extends HttpServlet {
    List<Product> productList = new ArrayList<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("product.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        int amount = Integer.parseInt(req.getParameter("amount"));
        int price = Integer.parseInt(req.getParameter("price"));

        productList.add(new Product(name, amount, price));
        req.setAttribute("productList", productList);
        req.getRequestDispatcher("product.jsp").forward(req, resp);
//        resp.sendRedirect("login.html");// navigate sang page khac nhung ko truyen data de xu ly tiep

    }

}
