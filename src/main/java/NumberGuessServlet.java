import model.Gamer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@WebServlet(name = "numberGuessServlet", urlPatterns = {"/numberGuess"})
public class NumberGuessServlet extends HttpServlet {
    List<Gamer> gamerList = new ArrayList<>();
    List<Gamer> doneGamerList = new ArrayList<>();
    Gamer currentGamer;
    int number = new Random().nextInt(1000);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("number", number);
        int guessNumber = req.getParameter("guessNumber") != null
                ? Integer.parseInt(req.getParameter("guessNumber")) : -1;

        String inputtedGamerName = req.getParameter("name");
        if(inputtedGamerName != null){
            currentGamer = new Gamer(inputtedGamerName);
            gamerList.add(currentGamer);
        } else {
            currentGamer = gamerList.get(gamerList.size() - 1);
            currentGamer.setGuessCount(currentGamer.getGuessCount() + 1);
        }

        // sắp xếp hạng khi người chơi đoán đúng
        if(number == guessNumber) {
            doneGamerList = gamerList.stream().sorted(Comparator.comparingInt(Gamer::getGuessCount)).collect(Collectors.toList());
            req.setAttribute("currentRank", doneGamerList.indexOf(currentGamer) + 1);
        }

        req.setAttribute("guessNumber", guessNumber);
        req.setAttribute("gamerList", doneGamerList);
        req.setAttribute("currentGamer", currentGamer);
        req.getRequestDispatcher("number-guess.jsp").forward(req, resp);

    }

}
