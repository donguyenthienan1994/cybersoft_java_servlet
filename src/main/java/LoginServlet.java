import model.Users;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        System.out.println("Login page");
//
//        //Ghi response tra ve cho client
//        PrintWriter printWriter = resp.getWriter();
//        printWriter.println("Hello Login Page");

        req.setAttribute("name","David Do");//Servlet input data can xu ly vao request thong qua variable va gui ra cho file JSP
        System.out.println("Email: " + req.getParameter("email"));
        System.out.println("Password: " + req.getParameter("password"));
        req.getRequestDispatcher("login.jsp").forward(req, resp);
//        resp.sendRedirect("login.html");// navigate sang page khac nhung ko truyen data de xu ly tiep

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Email: " + req.getParameter("email"));
        System.out.println("Password: " + req.getParameter("password"));
        Users users = new Users();
        users.setUsername(req.getParameter("email"));
        users.setPassword(req.getParameter("password"));
        req.setAttribute("user",users);

        List<String> list = Arrays.asList("Cybersoft","Java 21", "JSP");
        req.setAttribute("list", list);

        if(req.getParameter("email").equalsIgnoreCase("daviddo@gmail.com")){
            PrintWriter printWriter = resp.getWriter();
            printWriter.println("Welcome!!!!");
        }
        req.getRequestDispatcher("login.jsp").forward(req, resp);
    }
}
