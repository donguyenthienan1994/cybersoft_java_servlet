package codilityPractice;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;

public class CodilityExercise1 {
    public static void main(String[] args) {
        System.out.println(Solution("?xy1"));
    }

    public static int Solution(String S){
        Pattern pattern = Pattern.compile("[^a-zA-Z0-9]");
        List<String> list = new ArrayList<>();
        String[] arr = S.split(" ");
        for (String str : arr) {
            int digitCount = 0;
            int letterCount = 0;
            //Filter alphanumeric
            if(!pattern.matcher(str).find()){
                for (int i = 0; i < str.length(); i++) {
                    if (str.charAt(i) >= 48 && str.charAt(i) <= 57)
                        digitCount++;
                    else letterCount++;
                }
                if(digitCount % 2 > 0 && letterCount % 2 == 0) list.add(str);
            }
        }
        return list.size() > 0 ? list.stream().max(Comparator.comparingInt(String::length)).get().length() : -1;
    }
}
