package codilityPractice;

import java.util.*;
import java.util.regex.Pattern;

public class CodilityExercise2 {
    public static void main(String[] args) {
        System.out.println(Solution(new int[]{1,4,3,2,1,2,1,5,3,3,3,1,2}));
    }

    public static int Solution(int[] A){
        if(A.length <= 2) return 0;
        int leftHighestArea = -1;
        int leftHighestIndex = -1;
        int rightHighestArea = -1;
        int rightHighestIndex = -1;
        int deepestArea = 0;
        for (int i = 1; i < A.length - 1; i++) {

            int temp1 = A[i];
            int j;


            if(rightHighestIndex < i){
                rightHighestArea = -1;
            }

            if(rightHighestArea < 0){
                for (j = i + 1; j < A.length ; j++) {
                    if(A[j] > temp1) {
                        temp1 = A[j];
                        rightHighestArea = temp1;
                        rightHighestIndex = j;
                    }

                }
            }

//            rightHighestArea = Math.max(A[i],rightHighestArea);
            leftHighestArea = Math.max(A[i-1],leftHighestArea);
            if(rightHighestArea > 0 && leftHighestArea > 0){
                int new_deepestArea = Math.min(rightHighestArea, leftHighestArea) - A[i];
                deepestArea = Math.max(deepestArea, new_deepestArea);
            }
        }

        return deepestArea;
    }
}
