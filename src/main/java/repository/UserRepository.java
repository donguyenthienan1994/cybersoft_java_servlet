package repository;

import JDBC.config.MysqlConfig;
import model.UserModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {
    Connection connection = null;
    List<UserModel> userModelList;
    String sql = "";
    public List<UserModel> findByEmailAndPassword(String email, String password){
        userModelList = new ArrayList<>();
        try{
            connection = MysqlConfig.getConnection();
            sql = "SELECT * FROM users WHERE email = ? and password = ?;";
            PreparedStatement preparedStatement = MysqlConfig.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
//              Duyệt từng dòng dữ liệu
                UserModel userModel = new UserModel();
//              Lấy giá trị cột chỉ định
                userModel.setId(resultSet.getInt("id"));
                userModel.setEmail(resultSet.getString("email"));
                userModel.setFullname(resultSet.getString("fullname"));
                userModel.setAvatar(resultSet.getString("avatar"));
                userModel.setRoleId(resultSet.getInt("role_id"));

                userModelList.add(userModel);
            }
        } catch (Exception e){
            System.out.println("Error find by email and password: " + e.getMessage());
        } finally {
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    System.out.println("Lỗi đóng kết nối login " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }
        return userModelList;
    }

    public List<UserModel> findAllUsers(){
        userModelList = new ArrayList<>();
        try{
            connection = MysqlConfig.getConnection();
            sql = "SELECT * FROM users";
            PreparedStatement preparedStatement = MysqlConfig.getConnection().prepareStatement(sql);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
//              Duyệt từng dòng dữ liệu
                UserModel userModel = new UserModel();
//              Lấy giá trị cột chỉ định
                userModel.setId(resultSet.getInt("id"));
                userModel.setEmail(resultSet.getString("email"));
                userModel.setFullname(resultSet.getString("fullname"));
                userModel.setAvatar(resultSet.getString("avatar"));
                userModel.setRoleId(resultSet.getInt("role_id"));

                userModelList.add(userModel);
            }
        } catch (Exception e){
            System.out.println("Error find all users: " + e.getMessage());
        } finally {
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    System.out.println("Lỗi đóng kết nối login " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }
        return userModelList;
    }
}
