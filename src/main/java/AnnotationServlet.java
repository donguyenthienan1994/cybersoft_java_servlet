import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "annotationServlet", urlPatterns = {"/annotation"})
public class AnnotationServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // áp dụng mã UTF-8
        resp.setContentType("text/html;charset=UTF-8");
        //Ghi response tra ve cho client
        PrintWriter printWriter = resp.getWriter();
        printWriter.println("Đây là page servlet config bằng annotation");
        printWriter.println("Username: " + req.getParameter("username"));
        printWriter.println("Password: " + req.getParameter("password"));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Hello doPost");
    }
}
