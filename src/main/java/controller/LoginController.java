package controller;

import JDBC.config.MysqlConfig;
import model.UserModel;
import service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "loginController", urlPatterns = "/login")
public class LoginController extends HttpServlet {
    private LoginService loginService = new LoginService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        //Khoi tao cookie
//        Cookie cookie = new Cookie("username", "nguyenvana");
//        //Yeu cau client khoi tao cookie
//        resp.addCookie(cookie);

        String email = "";
        String password = "";

        for(Cookie cookie: req.getCookies()){
            if(cookie.getName().equals("email")) email = cookie.getValue();
            if(cookie.getName().equals("password")) password = cookie.getValue();
        }

        System.out.println(email);
        System.out.println(password);

        req.setAttribute("email", email);
        req.setAttribute("password", password);

        req.getRequestDispatcher("login.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

//        Bước 1: Lấy tham số username và password người dùng nhập
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String rememberPassword = req.getParameter("rememberPassword");
        System.out.println(rememberPassword);

        boolean isSuccess = loginService.checkLogin(email, password);

        if(isSuccess && rememberPassword != null){
            //Luu cookie
            Cookie cUserName = new Cookie("email", email);
            Cookie cPassword = new Cookie("password", password);

            resp.addCookie(cUserName);
            resp.addCookie(cPassword);
        }
//      Luu session, jsp file se dung cac attribute cua session luon
        session.setMaxInactiveInterval(60);
        session.setAttribute("s_email", email);
        session.setAttribute("s_password", password);
        session.setAttribute("s_isLogin", isSuccess);

        if(isSuccess){
//            req.getRequestDispatcher("/user").forward(req, resp);
            resp.sendRedirect(req.getContextPath() + "/user");
        } else {
            resp.sendRedirect("login.jsp");
        }
    }
}
