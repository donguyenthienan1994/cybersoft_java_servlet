# cybersoft_java_servlet

## Bai tap doan so

http://localhost:8080/javaservlet/inputGamer(Nhap ten nguoi choi)
http://localhost:8080/javaservlet/numberGuess(Bat dau tro choi)

## Mô hình MCV (mô hình cũ ko ai dùng nữa)

Model : Folder tương tác với lại database và lấy dữ liệu
View : Nơi xử lý logic code dể ra được data hiển thị lên UI
Controller : Nơi định nghĩa đường dẫn(link)

## Mô hình được dùng nhiều hiện nay (Chia theo package theo ý nghĩa chức năng - Apply cho Spring)

![](src/package_pattern.png)
